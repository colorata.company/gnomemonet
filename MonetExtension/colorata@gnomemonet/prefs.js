'use strict';

const GLib = imports.gi.GLib;
const ByteArray = imports.byteArray;
const Gtk = imports.gi.Gtk;
const Config = imports.misc.config;
const [major] = Config.PACKAGE_VERSION.split('.');
const shellVersion = Number.parseInt(major);
const gtkVersion = Gtk.get_major_version();
const Gio = imports.gi.Gio;
const Clutter = imports.gi.Clutter;

const ExtensionUtils = imports.misc.extensionUtils;
const Me = ExtensionUtils.getCurrentExtension();
/**
 * @type {MonetLib}
 */
const LibMonet = new Me.imports.MonetLib.MonetLib();
const GtkExtensions = new Me.imports.GtkExtensions.GtkExtensions();

function init() {
	log(`initializing ${Me.metadata.name} Preferences`);
}

function buildPrefsWidget() {
	let prefsWidget = new Gtk.Label({
		label: `${Me.metadata.name} Settings for Gnome ${gtkVersion}`
	});

	let slider1 = new Gtk.Scale({
		value_pos: Gtk.PositionType.LEFT
	});
	slider1.set_range(-35, 35);

	let slider2 = new Gtk.Scale({
		value_pos: Gtk.PositionType.LEFT
	});

	slider2.set_range(-35, 35);
	let headerBarText = new Gtk.Switch();
	headerBarText.set_active(false);
	let scroll = new Gtk.ScrolledWindow();
	scroll.set_policy(Gtk.PolicyType.NEVER, Gtk.PolicyType.AUTOMATIC);
	let window = new Gtk.FlowBox();
	window.set_valign(Gtk.Align.START);
	window.set_max_children_per_line(1);
	window.set_column_spacing(100);
	window.set_selection_mode(Gtk.SelectionMode.NONE);
	scroll.set_child(window);
	let text = new Gtk.Label({ label: "UnLoading" });
	let path = LibMonet.getWallpaper();
	text.set_label(LibMonet.getCurrentFile());
	//LibMonet.mainPredict();
	LibMonet.mainCompile();
	log(LibMonet.hsv2rgb(160, 70, 80));
	window.insert(slider1, 0);
	window.insert(prefsWidget, 1);
	window.insert(slider2, 2);
	window.insert(text, 3);
	let image = new Gtk.Image();
	image.set_from_file(path.substring(8, path.length - 2));
	image.set_pixel_size(300);
	window.insert(image, 4);
	let root = prefsWidget.get_root();
	/*GLib.idle_add(GLib.PRIORITY_DEFAULT, () => {
		let window;
		if (shellVersion >= 40) {
			window = scroll.get_root();
		} else {
			window = scroll.get_toplevel();
		}
		let headerBar = window.get_titlebar();
		headerBar.set_show_title_buttons(true);
		headerBar.title_widget = headerBarText;
		return GLib.SOURCE_REMOVE;
	});*/
	GtkExtensions.ModifyHeaderBar(scroll);

	return scroll;
}
