'use strict';

const GLib = imports.gi.GLib;
const ByteArray = imports.byteArray;
const Gtk = imports.gi.Gtk;
const Config = imports.misc.config;
const [major] = Config.PACKAGE_VERSION.split('.');
const shellVersion = Number.parseInt(major);
const gtkVersion = Gtk.get_major_version();
const Gio = imports.gi.Gio;
const Clutter = imports.gi.Clutter;

const ExtensionUtils = imports.misc.extensionUtils;
const Me = ExtensionUtils.getCurrentExtension();
const MarvinJ = Me.imports.MarvinJ;
/**
 * @type {MonetLib}
 */
 const LibMonet = new Me.imports.MonetLib.MonetLib();

class GtkExtensions {
    Image(url, size) {
        let image = new Gtk.Image();
	    image.set_from_file(url);
	    image.set_pixel_size(size);
        return image
    }
    Text(text) {
        return (new Gtk.Label({ label: text }));
    }
    Slider(minValue, maxValue) {
        let slider = new Gtk.Scale({
            value_pos: Gtk.PositionType.LEFT
        });
        slider.set_range(minValue, maxValue);
        return slider;
    }
    
    Switch(state) {
        let switchWidget = new Gtk.Switch();
        switchWidget.set_active(state);
        return switchWidget;
    }
    VerticalList(widgets, spacing) {
        let scroll = new Gtk.ScrolledWindow();
	    scroll.set_policy(Gtk.PolicyType.NEVER, Gtk.PolicyType.AUTOMATIC);
	    let window = new Gtk.FlowBox();
	    window.set_valign(Gtk.Align.START);
	    window.set_max_children_per_line(1);
	    window.set_column_spacing(spacing);
	    window.set_selection_mode(Gtk.SelectionMode.NONE);
	    scroll.set_child(window);
        widgets.forEach((widget, index) => {
            window.insert(widget, index);
        })
        return [scroll, window];
    }
    ModifyHeaderBar(root) {
        let button = new Gtk.Button({ icon_name: "view-refresh-symbolic"});
        button.connect('clicked', () => {
            log("160, 70, 80 = " + LibMonet.hsv2rgb(160, 70, 80));
        })
        GLib.idle_add(GLib.PRIORITY_DEFAULT, () => {
            let window;
            if (shellVersion >= 40) {
                window = root.get_root();
            } else {
                window = root.get_toplevel();
            }
            let headerBar = window.get_titlebar();
            headerBar.set_show_title_buttons(true);
            headerBar.pack_end(button);
            headerBar.title_widget = this.Text(Me.metadata.name);
            return GLib.SOURCE_REMOVE;
        });
    }
}