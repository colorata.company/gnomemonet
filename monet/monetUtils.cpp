#include <iostream>
#include <math.h>
#include <stdio.h>
#include <string>
#include <fstream>
#define PI 3.14159265
using namespace std;

string exec(const char* cmd) {
    array<char, 128> buffer;
    string result;
    unique_ptr<FILE, decltype(&pclose)> pipe(popen(cmd, "r"), pclose);
    if (!pipe) {
        throw runtime_error("popen() failed!");
    }
    while (fgets(buffer.data(), buffer.size(), pipe.get()) != nullptr) {
        result += buffer.data();
    }
    return result;
}

int luminancleFromRGB(int r, int g, int b) {
	return (int)((0.2126 * ((float)r)/255 + 0.7152 * ((float)g)/255 + 0.0722 * ((float)b)/255) * 100);
}

void replaceFile(string fileName, int countOfHashes, string to) {
	fstream file;
	file.open(fileName, ofstream::in | fstream::out);
	fstream out;
	string buffer;
	vector<string> fileFinal;
	int count = 0;
	int index = 0;
	int lineCounter = 0;
	int lineIndex = 0;
	bool isGot = false;
	while (getline(file, buffer)) {
		if (!isGot) {
			size_t finder = buffer.find('#');
			while (finder != string::npos) {
				count++;
				if (count == countOfHashes) { 
					index = finder;
					lineIndex = lineCounter;
					buffer.replace(index + 1, 6, to);
					isGot = true;
					break;
				}
				finder = buffer.find('#', finder + 6);
			}
		}
		fileFinal.push_back(buffer + "\n");
		lineCounter++;
	}
	file.close();
	ofstream newFile;
	newFile.open(fileName);
	for (int i = 0; i < fileFinal.size(); i++) {
		newFile << fileFinal[i];
	}
	newFile.close();
}

void compile() {
    string dir = __FILE__;
    string baseDir = dir.substr(0, dir.find_last_of("/"));
	exec(("sassc -M -t compact " + baseDir + "/Default-3/gtk-contained.scss " + baseDir + "/../MaterialLight/gtk-3.0/gtk.css").c_str());
	exec(("sassc -M -t compact " + baseDir + "/Default-3/gtk-contained-dark.scss " + baseDir + "/../MaterialDark/gtk-3.0/gtk.css").c_str());
	exec(("sassc -M -t compact " + baseDir + "/Default-4/Default-light.scss " + baseDir + "/../MaterialLight/gtk-4.0/gtk.css").c_str());
	exec(("sassc -M -t compact " + baseDir + "/Default-4/Default-dark.scss " + baseDir + "/../MaterialDark/gtk-4.0/gtk.css").c_str());
	exec(("sassc -M -t compact " + baseDir + "/Gnome-4/gnome-shell-light.scss " + baseDir + "/../MaterialLight/gnome-shell/gnome-shell.css").c_str());
	exec(("sassc -M -t compact " + baseDir + "/Gnome-4/gnome-shell-dark.scss " + baseDir + "/../MaterialDark/gnome-shell/gnome-shell.css").c_str());
	exec("gsettings set org.gnome.desktop.interface gtk-theme \"MaterialLight\"");
	exec("gsettings set org.gnome.desktop.interface gtk-theme \"MaterialDark\"");
	exec("gsettings set org.gnome.shell.extensions.user-theme name \"MaterialLight\"");
	exec("gsettings set org.gnome.shell.extensions.user-theme name \"MaterialDark\"");
}

string toHex(int r, int g, int b) {
	stringstream streamR;
	streamR << hex << r;
	
	stringstream streamG;
	streamG << hex << g;
	
	stringstream streamB;
	streamB << hex << b;
	string finalR = streamR.str();
	string finalG = streamG.str();
	string finalB = streamB.str();
	if (finalR.length() == 1) finalR = "0" + finalR;
	if (finalR.length() == 0) finalR = "00";
	if (finalG.length() == 1) finalG = "0" + finalG;
	if (finalG.length() == 0) finalG = "00";
	if (finalB.length() == 1) finalB = "0" + finalB;
	if (finalB.length() == 0) finalB = "00";
	return finalR + finalG + finalB;
}

class MonetObjects {
	public:
		class GTK4 {
			public:
				class Dark {
					public:
						static const int monetLightColor = 1;
						static const int monetDarkColor = 2;
						static const int monetForegroundColor = 3;
						static const int monetSuccessColor = 4;
						static const int monetDestructiveColor = 5;
						static const int monetAccent = 6;
						static const int monetOsdBackgroundColor = 7;
						static const int baseColor = 8;
						static const int textColor = monetLightColor;
						static const int backgroundColor = 10;
						static const int warningColor = 12;
						static const int errorColor = 13;
						static const int dropTargetColor = 15;
				};
				class Light {
					public:
						static const int monetLightColor = 1;
						static const int monetDarkColor = 2;
						static const int monetForegroundColor = 3;
						static const int monetSuccessColor = 4;
						static const int monetDestructiveColor = 5;
						static const int monetAccent = 6;
						static const int monetOsdBackgroundColor = 17;
						static const int baseColor = monetLightColor;
						static const int textColor = monetDarkColor;
						static const int backgroundColor = 9;
						static const int foregroundColor = 11;
						static const int warningColor = 12;
						static const int errorColor = 13;
						static const int dropTargetColor = 14;
				};
		};
		class GTK3 {
			public:
				class Dark {
					public:
						static const int monetLightColor = 1;
						static const int monetDarkColor = 2;
						static const int monetForegroundColor = 3;
						static const int monetSuccessColor = 4;
						static const int monetDestructiveColor = 5;
						static const int monetAccent = 6;
						static const int monetOsdBackgroundColor = 7;
						static const int baseColor = 8;
						static const int textColor = monetLightColor;
						static const int backgroundColor = 10;
						static const int warningColor = 12;
						static const int errorColor = 13;
						static const int dropTargetColor = 14;
				};
				class Light {
					public:
						static const int monetLightColor = 1;
						static const int monetDarkColor = 2;
						static const int monetForegroundColor = 3;
						static const int monetSuccessColor = 4;
						static const int monetDestructiveColor = 5;
						static const int monetAccent = 6;
						static const int monetOsdBackgroundColor = 17;
						static const int baseColor = monetLightColor;
						static const int textColor = monetDarkColor;
						static const int backgroundColor = 9;
						static const int foregroundColor = 11;
						static const int warningColor = 12;
						static const int errorColor = 13;
						static const int dropTargetColor = 14;
				};
		};
		class GNOME {
			public:
				class Dark {
					public:
						static const int monetLightColor = 1;
						static const int monetDarkColor = 2;
						static const int monetForegroundColor = 3;
						static const int monetSuccessColor = 7;
						static const int monetBackgroundColor = 4;
						static const int monetBaseColor = 5;
						static const int baseColor = 4;
						static const int monetAccent = 6;
						static const int destructiveColor = 8;
						static const int warningColor = 11;
						static const int errorColor = 12;
				};
				class Light {
					public:
						static const int monetLightColor = 1;
						static const int monetDarkColor = 2;
						static const int monetOsdForegroundColor = 3;
						static const int monetSuccessColor = 7;
						static const int monetOsdBackgroundColor = 4;
						static const int monetBaseColor = 5;
						static const int baseColor = 3;
						static const int monetAccent = 6;
						static const int backgroundColor = 9;
						static const int foregroundColor = 10;
						static const int destructiveColor = 8;
						static const int warningColor = 11;
						static const int errorColor = 12;
				};
		};
};


