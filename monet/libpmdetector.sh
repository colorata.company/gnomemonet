declare -A osInfo;
osInfo[/etc/redhat-release]=yum
osInfo[/etc/arch-release]=pacman
osInfo[/etc/SuSE-release]=zypp
osInfo[/etc/debian_version]=apt

for f in ${!osInfo[@]}
do
    if [[ -f $f ]];then
        echo ${osInfo[$f]}
    fi
done
#sudo apt install -y PACKAGE_NAME
#sudo yum -y install PACKAGE_NAME
#yes | sudo pacman -S PACKAGE_NAME
#sudo zypper -n install PACKAGE_NAME
